<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>

body {
  margin: 0;
  min-width: 250px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}

/* Remove margins and padding from the list */
ul {
  margin: 0;
  padding: 0;
}

/* Style the list items */
ul li {
  cursor: pointer;
  position: relative;
  padding: 12px 8px 12px 40px;
  list-style-type: none;
  background: #eee;
  font-size: 18px;
  transition: 0.2s;
  
  /* make the list items unselectable */
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Set all odd list items to a different color (zebra-stripes) */
ul li:nth-child(odd) {
  background: #f9f9f9;
}

/* Darker background-color on hover */
ul li:hover {
  background: #ddd;
}

/* When clicked on, add a background color and strike out text */
ul li.checked {
  background: #888;
  color: #fff;
  text-decoration: line-through;
}

/* Add a "checked" mark when clicked on */
ul li.checked::before {
  content: '';
  position: absolute;
  border-color: #fff;
  border-style: solid;
  border-width: 0 2px 2px 0;
  top: 10px;
  left: 16px;
  transform: rotate(45deg);
  height: 15px;
  width: 7px;
}

/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}

/* Clear floats after the header */
.header:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the input */
input {
  border: none;
  width: 75%;
  padding: 10px;
  float: left;
  font-size: 16px;
}

/* Style the "Add" button */
.addBtn {
  padding: 10px;
  width: 25%;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
}

.addBtn:hover {
  background-color: #bbb;
}
</style>
</head>
 <body>
        <?php
        $recibeDatos = $_POST['datos'];
        if($recibeDatos!="0"){
            $datos = explode(',',$recibeDatos);
            
            $con = mysqli_connect("localhost", "id4323386_calificador_tareas", "andrea02","id4323386_calificador_tareas");
            if(mysqli_connect_errno($con)){
                echo "Failed to connect to Mysql: " . mysqli_connect_error();
                exit(1);
            }
            $sql = "INSERT INTO `Maestro` (Nombre,Codigo) VALUES ('$datos[2]','$datos[1]')";
           
            if(!mysqli_query($con,$sql)){
                die('ERROR: ' . mysqli_error($con));
                echo "000";
            }
            mysqli_close($con);
            ?>
        !--Código: <?php echo $datos[1]; ?><br />
        !--Nombre: <?php echo $datos[2]; ?><br />
        !--Carrera: <?php echo $datos[4]; ?><br />
        !--Centro: <?php echo $datos[3]; ?><br />
            <?php
        }else{
            echo "Datos no válidos";
        }
        //json alumnos
        
        $url = "http://148.202.152.33/horarioMaestro.php";
        $json = file_get_contents($url);
        $json_data = json_decode($json, true);
        //var_dump($json_data);
        $con1 = mysqli_connect("localhost", "id4323386_calificador_tareas", "andrea02","id4323386_calificador_tareas");
            if(mysqli_connect_errno($con1)){
                echo "Failed to connect to Mysql: " . mysqli_connect_error();
                exit(1);
            }
        $ciclo=$json_data['ciclo'];
        for($i=0; $i<sizeof($json_data['horario']);$i++){
        $Crn= $json_data['horario'][$i]['crn'];
        $Campus=$json_data['horario'][$i]['campus'];
        $Seccion=$json_data['horario'][$i]['seccion'];
        $claveMateria=   $json_data['horario'][$i]['clave_materia'];
        $nombreMateria= $json_data['horario'][$i]['nombre_materia'];
        $horario=$json_data['horario'][$i]['dias'] ."-". $json_data['horario'][$i]['horario'];
        $Edificio= $json_data['horario'][$i]['edificio'] . " / " . $json_data['horario'][$i]['aula'];
        $Fecha=$json_data['horario'][$i]['fecha_inicio'] . " / " . $json_data['horario'][$i]['fecha_fin'];
        //echo $Crn. "  ". $Campus."  ".$Seccion."  ".$claveMateria." ".$nombreMateria." ".$horario."  ".$edificio."  ".$fecha;
        //echo "<br>";
        //fin grupos
        //inserciones
        
            $sql1 = "INSERT INTO `Materia` (Horario,Crn,ClaveMateria,Aula,NombreMateria) VALUES ('$horario','$Crn','$claveMateria','$Edificio','$nombreMateria')";
        
           
            if(!mysqli_query($con1,$sql1)){
                die('ERROR1: ' . mysqli_error($con1));
             }
            
            
            $alumnos = json_decode($json_data['horario'][$i]['alumnos']);
            foreach($alumnos as $alumno){
                
               $codigoAlumno= $alumno->codigo_alumno;
              $nombreAlumno=$alumno->nombre_alumno; 
              $carrera=$alumno->carrera;
              
               $sql2 = "INSERT INTO `Alumno` (Nombre,Carrera,Codigo) VALUES ('$nombreAlumno','$carrera','$codigoAlumno')";
        
           
            if(!mysqli_query($con1,$sql2)){
                die('ERROR1: ' . mysqli_error($con1));
             }
            }
          
        }
       mysqli_close($con1);
        
        
        
        ?>
        <div id="myDIV" class="header">
            <h2 style="margin:5px">Lista de grupos de: <?php echo $datos[2];  echo "<br>"; echo$datos[3];?></h2>
            <input type="text" id="myInput" placeholder="Horario...">
            <span onclick="newElement()" class="addBtn">Añadir</span>
        </div>
        
        <ul id="myUL">
            
 <script>
// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    var div = this.parentElement;
    div.style.display = "none";
  }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    document.getElementById("myUL").appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
    }
  }
}
      </script>
    </body>
</html>
       
